﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="WebFormsTest.Contact" %>


<%--Local registration--%>
<%--<%@ Register Src="~/Test.ascx" TagName="WebControl" TagPrefix="TestControl" %>--%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>

    <div id="form_person">
        <div class="form-group">
            <asp:Label ID="LabelFirstName" runat="server" >First Name</asp:Label>
            <asp:TextBox ID="FirstName" runat="server" class="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="FirstName" ErrorMessage="* Required"></asp:RequiredFieldValidator>
        </div>
        <div class="form-group">
            <asp:Label ID="LabelLastName" runat="server" >Last Name</asp:Label>
            <asp:TextBox ID="LastName" runat="server" class="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="LastName" ErrorMessage="* Required"></asp:RequiredFieldValidator>
        </div>
        <div class="form-group">
            <asp:Label ID="LabelAddress" runat="server" >Address</asp:Label>
            <asp:TextBox ID="Address" runat="server" class="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="Address" ErrorMessage="* Required"></asp:RequiredFieldValidator>
        </div>
        <div class="form-group">
            <label>City:</label>
            <asp:DropDownList ID="DropDownList" runat="server" CssClass="form-control">
                <asp:ListItem Text="Choose a city" Value="" />
                <asp:ListItem Text="Birmingham" Value="Birmingham" />
                <asp:ListItem Text="Manchester" Value="Manchester" />
                <asp:ListItem Text="Bristol" Value="Bristol" />
                <asp:ListItem Text="Nothingham" Value="Nothingham" />
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvDropDownList" runat="server" ControlToValidate="DropDownList" ErrorMessage="* Required"></asp:RequiredFieldValidator>
        </div>

        <asp:Button ID="SaveData" runat="server" Text="Save" class="btn btn-primary" OnClick="SaveData_Click" />
   </div>

    
    <div class="form-group" style="margin-top:25px;width:auto">
        <label>Your Details: </label>
        <asp:Literal ID="Details" runat="server"  />
    </div>
    
    <div class="content">
        <TestControl:WebControl ID="test" runat="server" Title="Test from Contact Page" />
    </div>

</asp:Content>
