﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFormsTest.Models
{
    public class Continent
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}