﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFormsTest.Models
{
    public enum Cost
    {
        Cheap,
        Moderate,
        Expensive
    }

    public class Destination
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public bool Visit { get; set; }
        public Cost Cost { get; set; }
    }
}