﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebFormsTest.Models;

namespace WebFormsTest
{
    public partial class Test : System.Web.UI.UserControl
    {
        public string Title { get; set; }
        public IEnumerable<Person> Persons { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<Person> persons = new List<Person>()
                {
                    new Person() { Id = 1, FirstName = "Tom", LastName = "Frog", Address = "8 Martial Street" },
                    new Person() { Id = 2, FirstName = "Jennifer", LastName = "Red", Address = "11 London Street" },
                    new Person() { Id = 3, FirstName = "Tony", LastName = "Abraham", Address = "20 Lucky Street" },
                    new Person() { Id = 4, FirstName = "Kelly", LastName = "Karen", Address = "14 Famine Street" }
                };

                Persons = persons.ToList();

                GridView1.DataSource = Persons;
                GridView1.DataBind();
            }

            //Title = "This is a Custom WebControl";
        }
    }
}