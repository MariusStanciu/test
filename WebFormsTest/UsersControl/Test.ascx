﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Test.ascx.cs" Inherits="WebFormsTest.Test" %>


<h2><%= Title %></h2>

<div class="container">
    <div class="row">
        <asp:GridView ID="GridView1" runat="server" CssClass="table table-hover" style="border:solid; border-color:#251d1d" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" />
                <asp:BoundField DataField="FirstName" HeaderText="FirstName"  />
                <asp:BoundField DataField="LastName" HeaderText="LastName"  />
                <asp:BoundField DataField="Address" HeaderText="Address"  />
            </Columns>

        </asp:GridView>
    </div>
</div>


