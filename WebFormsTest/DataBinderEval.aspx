﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataBinderEval.aspx.cs" Inherits="WebFormsTest.DataBinderEval" MasterPageFile="~/Site.Master" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <asp:Repeater ID="Repeater1" runat="server">
            <ItemTemplate>
                <p>
                <%# DataBinder.Eval(Container.DataItem, "FirstName") %> <%# DataBinder.Eval(Container.DataItem, "LastName") %>
                 <a href='<%# Eval("Id") %>'>Details</a>
                    </p>
                    <br />
            </ItemTemplate>
        </asp:Repeater>

        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" class="table table-sm">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Person ID" ItemStyle-Width="100px" />
                <asp:BoundField DataField="FirstName" HeaderText="First Name" ItemStyle-Width="130px" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" ItemStyle-Width="130px" />
                <asp:BoundField DataField="Address" HeaderText="Address" ItemStyle-Width="200px" />
                <asp:TemplateField ItemStyle-Width="70px">
                    <ItemTemplate>
                        <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary btn-sm" Text="Get Row Index" CommandArgument='<%# Container.DataItemIndex %>' OnClick="Button1_Click"/>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:TemplateField>
                    <ItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text=""></asp:TextBox>
                       
                    </ItemTemplate>
                </asp:TemplateField>--%>

            </Columns>
        </asp:GridView>
         <asp:Label ID="Label1" runat="server" Text="Row Index is " style="margin:20px; font-size:18px"></asp:Label>

    </div>

</asp:Content>