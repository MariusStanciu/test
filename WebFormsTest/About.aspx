﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="WebFormsTest.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <asp:GridView ID="gvPerson" runat="server" AutoGenerateColumns="false" BackColor="white" BorderColor="#3366cc" BorderStyle="Solid" BorderWidth="1px" CellPadding="4"
            onpageindexchanging="gvPerson_PageIndexChanging"
            onrowcancelingedit="gvPerson_RowCancelingEdit"
            onrowdatabound="gvPerson_RowDataBound"
            onrowdeleting="gvPerson_RowDeleting"
            onrowediting="gvPerson_RowEditing"
            onrowupdating="gvPerson_RowUpdating"
            onsorting="gvPerson_Sorting" OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
            Class="table table-hover" Style="margin: 2.5rem 0">
            <RowStyle BackColor="White" ForeColor="#003399" />
            <Columns>
                <asp:CommandField ShowEditButton="true" buttontype="Button" ControlStyle-CssClass="btn btn-outline-primary btn-sm"  />
                <asp:CommandField ShowDeleteButton="true" buttontype="Button" ControlStyle-CssClass="btn btn-danger btn-sm" />
                
                <asp:TemplateField HeaderText="" >
                    <ItemTemplate>
                        <asp:Button ID="editBtn" runat="server" Text="Edit" class="btn btn-secondary btn-sm" disabled="disabled"/>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="true" SortExpression="Id" />

                <asp:TemplateField HeaderText="First Name" SortExpression="FirstName" >
                   <%-- <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FirstName") %>' ></asp:TextBox>
                    </EditItemTemplate>--%>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("FirstName") %>'  OnClick="changeState(this);"></asp:Label>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FirstName") %>' Class="hidden form-control"  onchange="enableEdit(this);"
                            onfocusout="onFocusOut(this)" style="width:150px;"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
        
                <asp:TemplateField HeaderText="Last Name" SortExpression="LastName">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("LastName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Address" SortExpression="Address">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Address") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
            <FooterStyle BackColor="#99cccc" ForeColor="#003399" />
            <PagerStyle BackColor="#99cccc" ForeColor="#003399" HorizontalAlign="Left" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="true" ForeColor="#ccff99" />
            <HeaderStyle BackColor="#003399" Font-Bold="true" ForeColor="#ccccff" />

        </asp:GridView>
    </div>

    <asp:LinkButton ID="lbtnAdd" runat="server" OnClick="lbtnAdd_Click">Add New</asp:LinkButton>

    <asp:Panel ID="pnlAdd" runat="server" Visible="false">
        First Name:
        <asp:TextBox ID="tbFirstName" runat="server"></asp:TextBox>
        Last Name:
        <asp:TextBox ID="tbLastName" runat="server"></asp:TextBox>
        Address:
        <asp:TextBox ID="tbAddress" runat="server"></asp:TextBox>

        <asp:LinkButton ID="lbtnSubmit" runat="server" OnClick="lbtnSubmit_Click">Submit</asp:LinkButton>
        <asp:LinkButton ID="lbtnCancel" runat="server" OnClick="lbtnCancel_Click">Cancel</asp:LinkButton>
    </asp:Panel>

    <script>
        function changeState(label) {
            $(label).addClass('hidden').next().removeClass('hidden').focus();
        }

        function onFocusOut(textbox) {
            $(textbox).prev().removeClass('hidden');
            $(textbox).addClass('hidden');
        }

        function enableEdit(textbox) {
            $btn = $(textbox).closest('tr').find('td:eq(2) input');
            console.log($btn);
            $btn.attr("disabled", false);
        }

        </script>

</asp:Content>




