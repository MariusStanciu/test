﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebFormsTest.Models;

namespace WebFormsTest
{
    public partial class Contact : Page
    {
        string content = "Some text";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) {  }
                //TextBox.Text = content;
        }

        protected void SaveData_Click(object sender, EventArgs e)
        {
            if (!rfvFirstName.IsValid)
                SaveData.Enabled = false;

            Person person = new Person();

            person.Id = DateTime.Now.Millisecond;

            person.FirstName = FirstName.Text;
            person.LastName = LastName.Text;
            person.Address = Address.Text;

            Details.Text = $"{person.FirstName} {person.LastName}, Id: {person.Id} lives at {person.Address}, {DropDownList.SelectedValue}.";
        }
    }
}