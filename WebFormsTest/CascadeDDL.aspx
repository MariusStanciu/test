﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CascadeDDL.aspx.cs" Inherits="WebFormsTest.CascadeDDL" MasterPageFile="~/Site.Master" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container mt-5">
        <h3>Cascade DropDown List</h3>

        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Continet" class="mt-5"></asp:Label>
            <asp:DropDownList runat="server" CssClass="form-control" ID="Continents" AutoPostBack="true"
                DataTextField="Name" DataValueField="Id" OnSelectedIndexChanged="Continents_SelectedIndexChanged"></asp:DropDownList>
            
            <asp:Label ID="Label2" runat="server" Text="Country" class="mt-3"></asp:Label>
            <asp:DropDownList runat="server" CssClass="form-control" ID="Countries" Enabled="false"
                DataTextField="Name" DataValueField="Id" OnSelectedIndexChanged="Countries_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>

            <asp:Label ID="Label3" runat="server" Text="City" class="mt-3"></asp:Label>
            <asp:DropDownList runat="server" CssClass="form-control" ID="Cities" Enabled="false"
                DataTextField="Name" DataValueField="Id" AutoPostBack="True" OnSelectedIndexChanged="Cities_SelectedIndexChanged"></asp:DropDownList>
        </div>

        <div class="form-group">
            <h5>Selected Values are:</h5>
            <asp:Label ID="SelectedContinent" runat="server" Text="" style="display:block"></asp:Label>
            <asp:Label ID="SelectedCountry" runat="server" Text="" style="display:block"></asp:Label>
            <asp:Label ID="SelectedCity" runat="server" Text="" style="display:block"></asp:Label>
        </div>
    </div> 

</asp:Content>
