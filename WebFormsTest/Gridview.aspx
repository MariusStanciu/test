﻿<%@ Page Title="GriedView" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Gridview.aspx.cs" Inherits="WebFormsTest.Gridview" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">
        <asp:GridView ID="GridView1" runat="server" CssClass="table" Style="border-color: #cc3399"
             AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PageSize="4" AutoGenerateEditButton="true" DataKeyNames="Id">
            <Columns>
                <asp:BoundField ReadOnly="true" HeaderText="Id" DataField="Id" SortExpression="Id" />
                <asp:BoundField HeaderText="City" DataField="City" SortExpression="City" />
                <asp:BoundField HeaderText="Country" DataField="Country" SortExpression="Country" />
                <asp:BoundField HeaderText="Visit" DataField="Visit" SortExpression="Visit" />
                <asp:TemplateField HeaderText="Cost" >
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control">
                            <asp:ListItem Text="Cheap" Value="Cheap" />
                            <asp:ListItem Text="Moderate" Value="Moderate" />
                            <asp:ListItem Text="Expensive" Value="Expensive" />
                            <asp:ListItem Text="Select a Value" Value="" Selected="True"/>
                        </asp:DropDownList>
                    </ItemTemplate>
                  
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Label test" CssClass="title"></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server" Text="Test" CssClass="form-control"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Text="New dropDownList"></asp:Label>
            <br />
            <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control" Width="214px">
                <asp:ListItem Value="1">Value1</asp:ListItem>
                <asp:ListItem Value="2" selected="true">Value2</asp:ListItem>
                <asp:ListItem Value="3">Value3</asp:ListItem>
            </asp:DropDownList>
            <br />
        </div>

    </div>
</asp:Content>
