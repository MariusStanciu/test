﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebFormsTest.Models;

namespace WebFormsTest
{
    public partial class CascadeDDL : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetContinents();
            }
        }

        protected void Continents_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Continents.SelectedValue == "-1")
            {
                Countries.SelectedValue = "-1";
                Countries.Enabled = false;

                Cities.SelectedValue = "-1";
                Cities.Enabled = false;

                SelectedContinent.Text = "";
                SelectedCountry.Text = "";
                SelectedCity.Text = "";
                
            }
            else
            {
                getCountries(Int16.Parse(Continents.SelectedValue));
                Countries.Enabled = true;

                Cities.SelectedValue = "-1";
                Cities.Enabled = false;

                SelectedContinent.Text = Continents.SelectedItem.Text;
                SelectedCountry.Text = "";
                SelectedCity.Text = "";
            }
        }

        protected void Countries_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Countries.SelectedValue == "-1")
            {
                Cities.SelectedValue = "-1";
                Cities.Enabled = false;

                SelectedCountry.Text = "";
                SelectedCity.Text = "";
            }
            else
            {
                getCities(Int16.Parse(Countries.SelectedValue));
                Cities.Enabled = true;

                SelectedCountry.Text = Countries.SelectedItem.Text;
                SelectedCity.Text = "";
            }
        }

        protected void Cities_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cities.SelectedValue == "-1")
            {
                SelectedCity.Text = "";
            }
            else
            {
                SelectedCity.Text = Cities.SelectedItem.Text;
            }
        }

        private void GetContinents()
        {
            Continents.DataSource = GetData<Continent>(@"C:\Projects\Applications\WebFormsTest\WebFormsTest\Data\Continents.json");
            Continents.DataBind();
            ListItem SelectContinent = new ListItem("Select Continent", "-1");
            Continents.Items.Insert(0, SelectContinent);
        }

        private void getCountries(int id)
        {
            Countries.DataSource = GetData<Country>(@"C:\Projects\Applications\WebFormsTest\WebFormsTest\Data\Countries.json")
                .ToList().Where(x => x.ContinentId == id);
            Countries.DataBind();
            ListItem SelectCountry = new ListItem("Select Country", "-1");
            Countries.Items.Insert(0, SelectCountry);
        }

        private void getCities(int id)
        {
            Cities.DataSource = GetData<City>(@"C:\Projects\Applications\WebFormsTest\WebFormsTest\Data\Cities.json")
                .ToList().Where(x => x.CountryId == id);
            Cities.DataBind();
            ListItem SelectCity = new ListItem("Select City", "-1");
            Cities.Items.Insert(0, SelectCity);
        }

        private List<T> GetData<T>(string path) where T : class
        {
            var listOfData = new List<T>();

            using (StreamReader reader = new StreamReader(path))
            {
                string jsonData = reader.ReadToEnd();

                listOfData = JsonConvert.DeserializeObject<List<T>>(jsonData);
            }

            return listOfData;
        }

        
    }
}