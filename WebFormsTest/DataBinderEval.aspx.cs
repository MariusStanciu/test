﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebFormsTest.Models;

namespace WebFormsTest
{
    public partial class DataBinderEval : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var persons = new List<Person>();

            if (!IsPostBack)
            {
                persons.Add(new Person() { Id = 101, FirstName = "John", LastName = "Mark", Address = "56 Avenue Street " });
                persons.Add(new Person() { Id = 102, FirstName = "Will", LastName = "Smith", Address = "6 Corporation Street " });
                persons.Add(new Person() { Id = 103, FirstName = "Linda", LastName = "Jackson", Address = "23 Freedom Street " });
                persons.Add(new Person() { Id = 104, FirstName = "Marry", LastName = "Jane", Address = "9 Drawing Street " });

                Repeater1.DataSource = persons;
                Repeater1.DataBind();

                GridView1.DataSource = persons;
                GridView1.DataBind();
            }
           
            persons.Add(new Person() { Id = 101, FirstName = "John", LastName = "Mark", Address = "56 Avenue Street " });
            persons.Add(new Person() { Id = 102, FirstName = "Will", LastName = "Smith", Address = "6 Corporation Street " });
            persons.Add(new Person() { Id = 103, FirstName = "Linda", LastName = "Jackson", Address = "23 Freedom Street " });
            persons.Add(new Person() { Id = 104, FirstName = "Marry", LastName = "Jane", Address = "9 Drawing Street " });

            Label1.Text = "Row Index is ";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;

            // getting the row index
            string rowIndex = b.CommandArgument.ToString();

            Label1.Text += rowIndex.ToString();
        }

       
    }
}