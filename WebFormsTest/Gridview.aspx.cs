﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebFormsTest.Models;

namespace WebFormsTest
{
    public partial class Gridview : System.Web.UI.Page
    {
        public ICollection<Destination> Destinations { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            DataBindGridView();
         
        }

        private void DataBindGridView()
        {
            List<Destination> dest = new List<Destination>()
            {
                new Destination { Id = 101, City = "Rome", Country = "Italy", Visit = false, Cost = Cost.Moderate },
                new Destination { Id = 102, City = "Lisbon", Country = "Portugal", Visit = false, Cost = Cost.Cheap },
                new Destination { Id = 103, City = "Barcelona", Country = "Spain", Visit = true, Cost = Cost.Expensive },
                new Destination { Id = 104, City = "Paris", Country = "France", Visit = false, Cost = Cost.Expensive }
            };

            Destinations = dest;

            GridView1.DataSource = Destinations;
            GridView1.DataBind();
        }
    }
}