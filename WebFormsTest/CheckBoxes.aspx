﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckBoxes.aspx.cs" Inherits="WebFormsTest.CheckBoxes" MasterPageFile="~/Site.Master" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container mt-5">
        <h3>Check the cities you have visited</h3>
        
        <div class="form-group">
            <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_ItemCommand" >
            <ItemTemplate>
                <%--<asp:CheckBox ID='<%# "CheckBox" + Container.DataItem %>' runat="server" Text='<% #Container.DataItem %>' />--%>
                <%--<label class="checkbox">City:</label>--%>
                <asp:CheckBox ID="CheckBox1" runat="server" Text='<% #Container.DataItem %>' style="display:block"
                    OnCheckedChanged="CheckBox1_CheckedChanged" AutoPostBack="true"/>
                    
            </ItemTemplate>
        </asp:Repeater>
            <%--<asp:Literal ID="Literal1" runat="server" Text="Times is:"></asp:Literal>--%>
            <asp:CheckBox ID="CheckBox2" runat="server" OnCheckedChanged="CheckBox2_CheckedChanged" Text="Reset" AutoPostBack="true"/>
            <p>
                <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                </p>
        </div>
    </div>
</asp:Content>
