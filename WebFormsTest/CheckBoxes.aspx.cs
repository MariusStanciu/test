﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebFormsTest.Models;

namespace WebFormsTest
{
    public partial class CheckBoxes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Repeater1.DataSource = GetCities().Select(x => x.Name);
                Repeater1.DataBind();
            }
        }

        private List<City> GetCities()
        {
            return GetData<City>(@"C:\Projects\Applications\WebFormsTest\WebFormsTest\Data\Cities.json");
        }

        private List<T> GetData<T>(string path) where T : class
        {
            var listOfData = new List<T>();

            using (StreamReader reader = new StreamReader(path))
            {
                string jsonData = reader.ReadToEnd();

                listOfData = JsonConvert.DeserializeObject<List<T>>(jsonData);
            }

            return listOfData;
        }

        protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Response.Write("Event has been trigger");
        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox c = sender as CheckBox;

            if (c != null)
            {
                c.Enabled = false;
                Literal2.Text += $"<h5>{c.Text} has been selected.</h5> <br />";
            }

            CheckBox2.Checked = false;
        }

        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < Repeater1.Controls.Count; i++)
            {
                Control c = Repeater1.Controls[i];
                CheckBox checkBox = c.FindControl("CheckBox1") as CheckBox;
                if(checkBox != null)
                {
                    checkBox.Checked = false;
                    checkBox.Enabled = true;
                }
            }

            Literal2.Text = $"nr of cities is {Repeater1.Controls.Count}";
        }
    }
}