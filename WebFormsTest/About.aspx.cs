﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebFormsTest.Models;

namespace WebFormsTest
{
    public partial class About : Page
    {

        IList<Person> Persons;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Response.Write("Page loaded this is a post back");

                LoadData();

                gvPerson.AllowPaging = true;
                gvPerson.PageSize = 5;

                gvPerson.AllowSorting = true;

                ViewState["SortExpression"] = "Id ASC";

                BindGridView();
            }

            LoadData();
            Response.Write("Page loaded Not post back");
        }

        private void LoadData()
        {
            List<Person> persons = new List<Person>()
                {
                    new Person() { Id = 1, FirstName = "Tom", LastName = "Frog", Address = "8 Martial Street" },
                    new Person() { Id = 2, FirstName = "Jennifer", LastName = "Red", Address = "11 London Street" },
                    new Person() { Id = 3, FirstName = "Tony", LastName = "Abraham", Address = "20 Lucky Street" },
                    new Person() { Id = 4, FirstName = "Kelly", LastName = "Karen", Address = "14 Famine Street" },
                    new Person() { Id = 5, FirstName = "Ashley", LastName = "Truman", Address = "31 Malborow Street" },
                    new Person() { Id = 6, FirstName = "Laura", LastName = "Julien", Address = "4 Deep Street" }
                };

            Persons = persons.OrderBy(x => x.FirstName).ToList();
        }

        private void BindGridView()
        {
            gvPerson.DataSource = Persons;
            gvPerson.DataBind();
        }

        protected void gvPerson_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
            //    {
            //        ((Button)e.Row.Cells[1].Controls[0]).Attributes["onclick"] = "if (!confirm('Are you certain you want to delete this person ?')) return false;";
            //    }
            //}
        }

        protected void gvPerson_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPerson.PageIndex = e.NewPageIndex;

            BindGridView();
        }

        protected void gvPerson_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvPerson.EditIndex = e.NewEditIndex;

            BindGridView();

            lbtnAdd.Visible = false;
        }

        protected void gvPerson_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvPerson.EditIndex = -1;

            BindGridView();

            lbtnAdd.Visible = true;
        }

        protected void gvPerson_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int personId = Convert.ToInt16(gvPerson.Rows[e.RowIndex].Cells[2].Text);
            string firstName = ((TextBox)gvPerson.Rows[e.RowIndex].FindControl("TextBox1")).Text;
            string lastName = ((TextBox)gvPerson.Rows[e.RowIndex].FindControl("TextBox2")).Text;
            string address = ((TextBox)gvPerson.Rows[e.RowIndex].FindControl("TextBox3")).Text;

            var person = new Person()
            {
                Id = personId,
                FirstName = firstName,
                LastName = lastName,
                Address = address
            };

            Persons.Add(person);

            gvPerson.EditIndex = -1;

            BindGridView();

            lbtnAdd.Visible = true;
        }

        protected void gvPerson_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int personId = Convert.ToInt16(gvPerson.Rows[e.RowIndex].Cells[2].Text);
            var person = Persons.SingleOrDefault(x => x.Id == personId);

            if (person != null)
            {
                Persons.Remove(person);
            }

            BindGridView();
        }

        protected void gvPerson_Sorting(object sender, GridViewSortEventArgs e)
        {
            string[] strSortExpression = ViewState["SortExpression"].ToString().Split(' ');

            if (strSortExpression[0] == e.SortExpression)
            {
                if (strSortExpression[1] == "ASC")
                {
                    ViewState["SortExpression"] = e.SortExpression + " " + "DESC";
                }
                else
                {
                    ViewState["SortExpression"] = e.SortExpression + " " + "ASC";
                }
            }
            else
            {
                ViewState["SortExpression"] = e.SortExpression + " " + "ASC";
            }

            BindGridView();
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void lbtnAdd_Click(object sender, EventArgs e)
        {
            lbtnAdd.Visible = false;
            pnlAdd.Visible = true;
        }

        protected void lbtnSubmit_Click(object sender, EventArgs e)
        {
            int personId = Persons.Last().Id;
            string firstName = tbFirstName.Text;
            string lastName = tbLastName.Text;
            string address = tbAddress.Text;

            var person = new Person()
            {
                Id = personId,
                FirstName = firstName,
                LastName = lastName,
                Address = address
            };

            Persons.Add(person);

            BindGridView();

            lbtnCancel_Click(sender, e);
        }

        protected void lbtnCancel_Click(object sender, EventArgs e)
        {
            tbFirstName.Text = "";
            tbLastName.Text = "";
            tbAddress.Text = "";

            lbtnAdd.Visible = true;
            pnlAdd.Visible = false;
        }

        protected void filedClicked(object sender, GridViewUpdateEventArgs e)
        {
            Response.Write("First Name field has been pressed");

            ((TextBox)gvPerson.Rows[e.RowIndex].FindControl("TextBox1")).ReadOnly = false;
        }

    }
}